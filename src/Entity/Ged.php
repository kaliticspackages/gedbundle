<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ged
 *
 * @ORM\Table(name="ged_ged")
 * @ORM\Entity(repositoryClass="Kalitics\GedBundle\Repository\GedRepository")
 */
class Ged
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, unique=false)
     */
    private $path;

    /**
     * Many GED have Many Documents.
     * @ORM\OneToMany(targetEntity="Kalitics\GedBundle\Entity\Document", mappedBy="ged")
     */
    private $documents;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\DocumentCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\ConfigGedCategory")
     * @ORM\JoinColumn(name="config_id", referencedColumnName="id")
     */
    private $config;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isGalleryMode = false;

    /**
     * Ged constructor.
     * @param $documents
     */
    public function __construct($path = "")
    {
        $this->documents    = new ArrayCollection();
        $this->path         = $path;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Ged
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     */
    public function setDocuments($documents): void
    {
        $this->documents = $documents;
    }

    /**
     * @param \Kalitics\GedBundle\Entity\Document $document
     */
    public function addDocument(Document $document) : void {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
        }

        $document->setGed($this);
    }

    /**
     * @param \Kalitics\GedBundle\Entity\Document $document
     */
    public function removeDocument(Document $document) : void {
        $this->documents->removeElement($document);

        if ($document->getGed() === $this) {
            $document->setGed(null);
        }
    }

    /**
     * @return \Kalitics\GedBundle\Entity\DocumentCategory|null
     */
    public function getCategory(): ?DocumentCategory
    {
        return $this->category;
    }

    /**
     * @param \Kalitics\GedBundle\Entity\DocumentCategory|null $category
     */
    public function setCategory(
        ?DocumentCategory $category
    ) : void {
        $this->category = $category;
    }

    /**
     * @return \Kalitics\GedBundle\Entity\ConfigGedCategory|null
     */
    public function getConfig(): ?ConfigGedCategory
    {
        return $this->config;
    }

    /**
     * @param \Kalitics\GedBundle\Entity\ConfigGedCategory|null $config
     */
    public function setConfig(
        ?ConfigGedCategory $config
    ) : void {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isGalleryMode(): bool
    {
        return $this->isGalleryMode;
    }

    /**
     * @param bool $isGalleryMode
     */
    public function setIsGalleryMode(bool $isGalleryMode): void
    {
        $this->isGalleryMode = $isGalleryMode;
    }
}
