<?php

namespace Kalitics\GedBundle\Controller;

use Kalitics\GedBundle\Entity\DocumentCategory;
use Kalitics\GedBundle\Entity\DocumentType;
use Kalitics\GedBundle\Entity\Ged;
use Kalitics\GedBundle\Service\GedConfigurationService;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class GedController extends AbstractController
{

    public function displayAction(
        GedConfigurationService $gedConfigurationService,
        $entity,
        $entityField = null
    ) {
        $em = $this->getDoctrine()->getManager();

        $getter = $entityField ?: 'ged';

        $reflection = new ReflectionClass($entity);
        if (!$reflection->hasMethod('get'.ucfirst($getter))) {
            throw new \Exception('Ged association missing ? Called '. get_class($entity) . '->get'.ucfirst($getter));
        }

        if ($entity->{'get'.ucfirst($getter)}() === null) {
            $entity = $gedConfigurationService->configureGed($entity, $entityField);

            $em->persist($entity);
            $em->flush();
        }

        return $this->render('@KaliticsGed/Ged/display.html.twig', array(
            "ged" => $entity->{'get'.ucfirst($getter)}()
        ));
    }

    public function displayFileListAction($id){
        $em = $this->getDoctrine()->getManager();
        $ged = $em->getRepository(Ged::class)->find($id);

        return $this->render('@KaliticsGed/_includes/_list_files.html.twig', array(
            "ged" => $ged
        ));
    }

    public function displayFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $ged = $em->getRepository(Ged::class)->find($id);

        return $this->render('@KaliticsGed/_includes/_upload_file.html.twig', array(
            "ged"               => $ged,
            "documentsTypes"    => $this->getDocumentTypesFromGed($ged)
        ));
    }

    public function displayModeAction($id,$isGalleryMode){
        $em = $this->getDoctrine()->getManager();

        /** @var Ged $ged */
        $ged = $em->getRepository(Ged::class)->find($id);

        if (!$ged) {
            return new JsonResponse("ged not found", 404);
        }

        $ged->setIsGalleryMode(filter_var($isGalleryMode, FILTER_VALIDATE_BOOLEAN));

        $em->persist($ged);
        $em->flush();

        return $this->render('@KaliticsGed/_includes/_list_files.html.twig', array(
            "ged" => $ged
        ));
    }

    public function getDocumentTypesFromGed(Ged $ged){
        $em = $this->getDoctrine()->getManager();

        $category = $ged->getCategory();

        $criteria = [];

        if ($category instanceof DocumentCategory) {
            $criteria['category'] = $category;
        }

        return $em->getRepository(DocumentType::class)->findBy(
            $criteria,
            [
                'name' => 'ASC',
            ],
        );
    }

}
