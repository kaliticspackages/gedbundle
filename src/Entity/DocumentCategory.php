<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="ged_document_category")
 * @ORM\Entity(repositoryClass="Kalitics\GedBundle\Repository\CategoryRepository")
 */
class DocumentCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Kalitics\GedBundle\Entity\DocumentType", mappedBy="category")
     */
    private $documentTypes;

    public function __construct(){
        $this->documentTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DocumentCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getDocumentTypes()
    {
        return $this->documentTypes;
    }

    /**
     * @param mixed $documentTypes
     */
    public function setDocumentTypes($documentTypes)
    {
        $this->documentTypes = $documentTypes;
    }

    public function addDocumentType(DocumentType $documentType): self
    {
        if (!$this->documentTypes->contains($documentType)) {
            $this->documentTypes[] = $documentType;
            $documentType->setCategory($this);
        }

        return $this;
    }

    public function removeDocumentType(DocumentType $documentType): self
    {
        if ($this->documentTypes->contains($documentType)) {
            $this->documentTypes->removeElement($documentType);
            // set the owning side to null (unless already changed)
            if ($documentType->getCategory() === $this) {
                $documentType->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getName();
    }
}
