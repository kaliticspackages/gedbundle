<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Utilities\EnumEntity;
use App\Entity\Utilities\Traits\BlameableEntity;

/**
 * DocumentType
 *
 * @ORM\Table(name="ged_document_type")
 * @ORM\Entity(repositoryClass="Kalitics\GedBundle\Repository\DocumentTypeRepository")
 */
class DocumentType extends EnumEntity
{
    use BlameableEntity;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\DocumentCategory", inversedBy="documentTypes")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Kalitics\GedBundle\Entity\DocumentSubType", mappedBy="type")
     */
    private $documentSubTypes;


    public function __construct()
    {
        parent::__construct();

        $this->documentSubTypes = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return Collection|DocumentSubType[]
     */
    public function getDocumentSubTypes(): Collection
    {
        return $this->documentSubTypes;
    }

    public function addDocumentSubType(DocumentSubType $documentSubType): self
    {
        if (!$this->documentSubTypes->contains($documentSubType)) {
            $this->documentSubTypes[] = $documentSubType;
            $documentSubType->setType($this);
        }

        return $this;
    }

    public function removeDocumentSubType(DocumentSubType $documentSubType): self
    {
        if ($this->documentSubTypes->contains($documentSubType)) {
            $this->documentSubTypes->removeElement($documentSubType);
            // set the owning side to null (unless already changed)
            if ($documentSubType->getType() === $this) {
                $documentSubType->setType(null);
            }
        }

        return $this;
    }
}
