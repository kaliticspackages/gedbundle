<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Document
 *
 * @ORM\Table(name="ged_document")
 * @ORM\Entity(repositoryClass="Kalitics\GedBundle\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(name="validity", type="datetime", nullable=true)
     */
    private $validity;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\DocumentType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\DocumentSubType")
     * @ORM\JoinColumn(name="subtype_id", referencedColumnName="id")
     */
    private $subType;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\Ged", inversedBy="documents")
     * @ORM\JoinColumn()
     */
    private $ged;

    /**
     * Document constructor.
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getGed()
    {
        return $this->ged;
    }

    /**
     * @param mixed $ged
     */
    public function setGed($ged)
    {
        $this->ged = $ged;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * @param \DateTime $validity
     */
    public function setValidity($validity)
    {
        $this->validity = $validity;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getPath();
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getWeight(){
        if(file_exists($this->getPath())){
            return filesize($this->getPath());
        }else{
            return 0;
        }
    }

    public function getDispWeight(){

        $bytes = $this->getWeight();

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    public function getExtension(){
        $path = $this->getPath();
        $pathElements  = new ArrayCollection(explode('.', $path));
        return $pathElements->last();
    }

    //Retour du type "Nomdocument (jpeg) Expire le 28/01/1990"
    public function __toString(){
        $this->validity != null ? $valid = "Expire le ".$this->validity->format('d/m/Y') : $valid = "";
        return $this->getName()." (".pathinfo($this->getPath(), PATHINFO_EXTENSION).") ".$valid;
    }

    public function getSubType(): ?DocumentSubType
    {
        return $this->subType;
    }

    public function setSubType(?DocumentSubType $subType): self
    {
        $this->subType = $subType;

        return $this;
    }
}

