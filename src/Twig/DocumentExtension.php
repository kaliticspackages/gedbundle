<?php


namespace Kalitics\GedBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Kalitics\GedBundle\Entity\Document;


class DocumentExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('urlImage', [$this, 'getUrlImage']),
            new TwigFilter('isImg', [$this, 'isDocumentAnImage']),
            new TwigFilter('isPdf', [$this, 'isDocumentPdf']),
            new TwigFilter('docIcon', [$this, 'documentIcon']),
            new TwigFilter('printType', [$this, 'printType'])
        ];
    }

    public function getUrlImage(Document $document) : string
    {
        $package = new Package(new EmptyVersionStrategy());
        $url = "";
        switch ($document->getExtension()) {
            case 'png':
            case 'jpg':
            case 'jpeg':
                if (file_exists($document->getPath())){
                    $url = $package->getUrl('/'.$document->getWebPath());
                }else{
                    $url = $package->getUrl('/bundles/kaliticsged/images/jpg.png');
                }
                break;
            case 'pdf':
                $url = $package->getUrl('/bundles/kaliticsged/images/pdf.png');
                break;
            default:
                $url = $package->getUrl('/bundles/kaliticsged/images/doc.png');
        }

        return $url;
    }

    public function isDocumentAnImage(Document $document) : bool {
        $supported_image = ['jpg', 'jpeg', 'png'];

        return in_array($document->getExtension(), $supported_image, true);
    }

    public function isDocumentPdf(Document $document) : bool {
        return $document->getExtension() === "pdf";
    }

    public function documentIcon(Document $document) {
        switch ($document->getExtension()){
            case 'pdf':
                return '<i class="far fa-file-pdf" style="color: #fd397a;font-size: 16px"></i>';
            case 'png':
            case 'jpg':
            case 'jpeg':
                return '<i class="far fa-file-image" style="color: #006689;font-size: 16px"></i>';
        }
    }

    public function printType(Document $document) {
        switch ($document->getExtension()){
            case 'pdf':
                return 'pdf';
            case 'png':
            case 'jpg':
            case 'jpeg':
                return 'image';
            case 'html':
                return 'html';
            case 'json':
                return 'json';
        }
    }
}
