<?php

namespace Kalitics\GedBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Kalitics\GedBundle\Entity\ConfigGedCategory;
use Kalitics\GedBundle\Entity\DocumentCategory;
use Kalitics\GedBundle\Entity\Ged;


class GedConfigurationService
{

    /** @var \Doctrine\ORM\EntityManagerInterface  */
    private EntityManagerInterface $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function configureGed($entity, $fieldName = null, $path = "")
    {

        if($fieldName != null){
            $config = $this->entityManager->getRepository(ConfigGedCategory::class)->findOneBy(array(
                "entityField" => get_class($entity).":".$fieldName
            ));
        }else{
            $config = $this->entityManager->getRepository(ConfigGedCategory::class)->findOneBy(array(
                "entityField" => get_class($entity)
            ));

            if (!$config && method_exists($entity, 'getGedConfigEntity')) {
                $config = $this->entityManager->getRepository(ConfigGedCategory::class)->findOneBy([
                    'entityField' => $entity->getGedConfigEntity()
                ]);
            }
        }

        $category = null;
        if($config instanceof ConfigGedCategory){
            $category = $config->getCategory();
        }

        $ged = new Ged($path);
        $ged->setCategory($category);
        $ged->setConfig($config);

        if($fieldName == null){
            $entity->setGed($ged);
        }else{
            $entity->{'set'.ucfirst($fieldName)}($ged);
        }

        return $entity;
    }
}
