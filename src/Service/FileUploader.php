<?php

namespace Kalitics\GedBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Image;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class FileUploader
{
    private string $targetDirectory;

    public function __construct(string $targetDir)
    {
        $this->targetDirectory  = $targetDir;
    }

    public function upload(UploadedFile $file, $destination = "", $filePrefix = "")
    {
        $fileName = $filePrefix.md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->getTargetDirectory().'/'.$destination, $fileName);
        return $this->getTargetDirectory().''.$destination.''.$fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

}
