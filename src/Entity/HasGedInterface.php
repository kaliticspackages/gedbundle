<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

interface HasGedInterface
{
    /**
     * @return \Kalitics\GedBundle\Entity\Ged|null
     */
    public function getGed();

    /**
     * @param \Kalitics\GedBundle\Entity\Ged $ged
     */
    public function setGed($ged);
}
