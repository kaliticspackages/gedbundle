<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigGedCategory
 *
 * @ORM\Table(name="ged_config_ged_category")
 * @ORM\Entity(repositoryClass="Kalitics\GedBundle\Repository\ConfigGedCategoryRepository")
 */
class ConfigGedCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_ged", type="string", length=255, unique=true)
     */
    private $entityField;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\DocumentCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEntityField()
    {
        return $this->entityField;
    }

    /**
     * @param string $entityField
     */
    public function setEntityField($entityField)
    {
        $this->entityField = $entityField;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->getName();
    }
}
