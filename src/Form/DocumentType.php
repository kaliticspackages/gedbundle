<?php

namespace Kalitics\GedBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', EntityType::class, [
                'label'             => "Type de document",
                'required'          => true,
                'class'             => 'Kalitics\GedBundle\Entity\DocumentType',
                'attr'              => array('class'=>''),
                'choices'           => $options['documentsTypes']
            ])
            ->add('subtype', EntityType::class, [
                'label' => 'Sous-type',
                'required' => false,
                'class' => 'Kalitics\GedBundle\Entity\DocumentSubType',
                'attr' => array('class'=>''),
                'placeholder' => 'Sélectionner un sous-type',
                'choice_attr' => function($choice, $key, $value) {
                    return [
                        'data-type' => $choice->getType()->getId(),
                    ];
                }
            ])
            ->add('validity', DateType::class, array(
                'label'     => "Date d'entrée",
                'widget'    => 'single_text',
                "required"  => false,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kalitics\GedBundle\Entity\Document'
        ));
        $resolver->setDefined(array('documentsTypes'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gedbundle_document';
    }


}
