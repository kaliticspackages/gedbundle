File management for Symfony applications

##Installation

Require the bundle using 

```
composer require kalitics/ged-bundle
```

Create the file config/packages/kalitics_ged.yaml with the following content :

```
kalitics_ged:
    path: "/"
```

Create a new file config/routes/kalitics_ged.yaml

```
_kalitics_ged:
  resource: '@GedBundle/Resources/config/routes.xml'
  prefix: /kalitics/kaliticsged/
```


##Configuration

When creating an entity which own a GED, use the service `\Kalitics\GedBundle\Service\GedConfigurationService` to configure the GED on the new entity.

```
$entity = $service->configureGed($entity);
```

To get properly configured, the database need some informations such as Categories, and DocumentTypes. The ged is configured to display the proper categories in forms. If not configured, the entity will display all the documents types in the add and update form.
It's important to link the right catagorie to the gedOwnerEntity by adding some informations in the ``ged_config_ged_category`` table of the database.
The entity Name must be ``Namespace\EntityName`` and the categorie the ``id`` of the categorie.


##Usage

Tag an entity as ower of a file management adding a use in the entity class :

```
use GedOwnerTrait;
```

Display the file namagement in a template rendering the GedController :

```
{{ render(controller('kalitics_ged.controller.ged_controller:displayAction', {'entity': project})) }}
```

### Rename in config
Rename from Ressources to Resources

###Install assets bundles
```
php bin/console assets:install
```
###Change thumbnail ratio image
You can add in **services.yaml** this to change scale image  
more the value is low more the image is light but also more the image is pixelated
```yaml
ged_thumbnail_image_scale: 0.7
```
default value if no parameter is 0.5

### Add configuration for gaufrette bundle  
``` config/packages/knp_graufrette.yaml```
```yaml
knp_gaufrette:
  adapters:
    ged_thumbnails:
      local:
        directory: "%kernel.project_dir%/public/bundles/kaliticsged/images/thumbnail"
        create: true
   filesystems:
      ged_thumbnails_fs:
        adapter: ged_thumbnails
        alias: ged_thumbs_fs
```

### Imagick required
If you don't have imagick already:  
Create a ```script.sh```, then add (script for php7.3):
```shell
#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

apt-get install imagemagick pkg-config libmagickwand-dev -y
cd /tmp
wget https://pecl.php.net/get/imagick-3.4.3.tgz
tar xvzf imagick-3.4.3.tgz
cd imagick-3.4.3
#install php7.3-dev to use phpize
apt-get install php7.3-dev -y
#compile extension
phpize
./configure
make install
#Delete folder after compilation
rm -rf /tmp/imagick-3.4.3*
#Add to php.ini and restart to apply change
echo extension=imagick.so >> /etc/php/7.3/cli/php.ini
service apache2 restart
```
then run your script ```sudo bash script.sh```
