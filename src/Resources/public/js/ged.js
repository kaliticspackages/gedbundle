
function reloadFileList(gedId){
    let fileList = $('#ged_'+gedId+'_list_files');
    if (fileList.length) {
        fileList.load(fileList.data('url'), function () {
            $("#get_documents_list-"+gedId).DataTable();
            $("#get_documents_list-"+gedId+"_filter input[type=search]").css('display','block');
        });
    } else {
        window.location.href = window.location.href;
    }
}

function deleteDocument(idDocument, gedId){

    let path = $('#tr_document_'+idDocument).data('remove');


    swal.fire({
        title: "Etes vous sur ?",
        text: "Vous êtes sur le point de supprimer un document",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Supprimer",
        cancelButtonText: "Annuler",
        closeOnConfirm: false
    }).then((result) => {

        if(result.value == true){
            $.ajax({
                url: path,
                type: 'POST',
                data: {},
                cache: false,
                contentType: false,
                processData: false,
                success:function (data) {
                },
                error:function () {
                    swal("Erreur :(", "Une erreur est survenue lors de l'enregistrement. Contactez un administrateur.", "error");
                },
                complete:function () {
                    reloadFileList(gedId);
                },

            });
        }else{
        }
    });
}


function updateDocument(idDocument, gedId){

    let path = $('#tr_document_'+idDocument).data('update');

    $('#modal_documents').modal('show');

    $.ajax({
        url: path,
        type: 'GET',
        data: {},
        success: function(response){
            $('#modal_documents .modal-content').html(response);

            let inputDate = $('#gedbundle_document_validity');

        },
        error:function () {
            swal("Erreur :(", "Une erreur est survenue lors de l'enregistrement. Contactez un administrateur.", "error");
        },
        complete:function () {
        },
    });
}

function sendFormUpdateDocument(gedId){


    let form = $("#update_document_form_container form");

    var formData = new FormData(form[0]);

    $('#loader').show();
    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        error:function(err){
        },
        success:function(data){

        },
        complete:function(){
            reloadFileList(gedId);
            $('#modal_documents').modal('hide');
        }
    });
}

function initGedZone() {

    $('.bh-ged-type-select').change(function() {
        if ($('.bh-ged-subtype-select option[data-type='+ $(this).val() +']').length) {
            $('.bh-ged-subtype').show();
        } else {
            $('.bh-ged-subtype').hide();
            $('.bh-ged-subtype-select').val('');
        }

    }).trigger('change');

    $('.ged-dropzone').each(function( index ) {
        console.log("instanciate dropzone ged");
        //drop('site', $( this ).data('id'));

        $( this ).addClass('dropzone');
        let gedId = $( this ).data('id-ged');

        var formData = new FormData();
        formData.append('file',$('#get_form_'+gedId)[0]);
        try {
            var myDropzone = new Dropzone("div#dropzone_ged_" + gedId, {
                url: $(this).data('upload-url'),
                type: 'POST',
                data: formData,
                addRemoveLinks: true,
                autoProcessQueue: false,
                acceptedFiles: ".jpeg,.jpg,.png,.pdf",
                init: function () {
                    this.on("addedfile", function (file) {
                        $('#upload_btn_' + gedId).show();
                        if (this.files[1] != null) {
                            this.removeFile(this.files[0]);
                        }
                    });
                    this.on('sending', function (file, xhr, formData) {
                        //Append all form inputs to the formData Dropzone will POST
                        var data = $('#ged_form_' + gedId).serializeArray();
                        $.each(data, function (key, el) {
                            formData.append(el.name, el.value);
                        });
                    });
                },
                success: function (file, response) {
                    this.removeFile(this.files[0]);
                    reloadFileList(gedId);
                },
            });
        } catch(e) {}

        $("#upload_btn_"+gedId).click(function (e) {
            e.preventDefault();
            var myDropzone = Dropzone.forElement("#dropzone_ged_"+gedId);
            myDropzone.processQueue();
        });

    });
}

$(document).ready(function() {
    initGedZone();
});
