<?php

namespace Kalitics\GedBundle\Entity;

use App\Entity\Utilities\EnumEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentType
 *
 * @ORM\Table(name="ged_document_sub_type")
 * @ORM\Entity(repositoryClass="Kalitics\GedBundle\Repository\DocumentSubTypeRepository")
 */
class DocumentSubType extends EnumEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\GedBundle\Entity\DocumentType", inversedBy="documentSubTypes")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
