<?php

namespace Kalitics\GedBundle\Controller;

use Gaufrette\Adapter\Local;
use Gaufrette\Filesystem;
use Kalitics\GedBundle\Entity\Document;
use Kalitics\GedBundle\Entity\DocumentSubType;
use Kalitics\GedBundle\Entity\DocumentType;
use Kalitics\GedBundle\Form\DocumentType as FormDocument;
use Kalitics\GedBundle\Entity\Ged;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use function Couchbase\defaultDecoder;

class DocumentController extends AbstractController
{
    const IMAGE_RES = 100;
    //Liste des caractéres à remplacer par des "_" dans les noms de fichiers pour les téléchargements
    static $filenameReplaceArray = array(
        '\\','/'
    );

    public function uploadAction(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $ged        = $em->getRepository(Ged::class)->find($id);

        //Parameters
        $file 			= $request->files->get('file');
        $name			= $request->request->get('file_name');
        $type			= $request->request->get('file_type');
        $subtype = $request->request->get('file_subtype');
        $validDate		= $request->request->get('file_validity');

        //Build attributes
        $validDate      = $validDate ? new \DateTime($validDate) : null;
        $type           = $em->getRepository(DocumentType::class)->find($type);
        if ($subtype) {
            $subtype = $em->getRepository(DocumentSubType::class)->find($subtype);
        }

        if($name == "" && $type != null){
            $name = $type->getName();
        }

        //Create document
        $fileName       = $this->saveFile($file);
        $document       = new Document($fileName);
        $document->setName($name);
        $document->setValidity($validDate);
        $document->setGed($ged);

        if($type != null){
            $document->setType($type);
        }

        if ($subtype !== null) {
            $document->setSubType($subtype);
        }

        $em->persist($document);
        $em->flush();

        return new JsonResponse($document, Response::HTTP_OK);
    }

    public function showAction(int $id, $thumbnail) {
        /** @var Document $document */
        $document   = $this->getDoctrine()->getRepository(Document::class)->find($id);
        $ratio = 0.5;
        try {
            $ratio = $this->container->getParameter("ged_thumbnail_image_scale");
        }catch (\Exception $e){

        }

        if ($ratio <=0 || $ratio >1)
        {
            throw new \Exception("Error thumbnail ratio must be between 0 and 1 [0%, 100%]");
        }

        /** @var Filesystem $filesystem */
        $filesystem    = $this->container->get('ged_thumbs_fs');
        $url           = $this->getRessourceUrl($document);
        $directoryPath = $this->getDirectoryPath($filesystem);

        $thumbUrl = $directoryPath.'/thumb-'.$document->getId().'.jpg';
        $thumbName = '/thumb-'.$document->getId().'.jpg';

        if ($thumbnail && $filesystem->getAdapter()->exists($thumbName)){
            return new BinaryFileResponse(new File($thumbUrl));
        }

        if ($thumbnail && !$filesystem->getAdapter()->exists($thumbName)) {
            $imagick = new \Imagick();
            $imagick->readImage($document->getWebPath() . '[0]');
            $imagick->scaleimage(
                $imagick->getImageWidth() * $ratio,
                $imagick->getImageHeight() * $ratio
            );
            $imagick->setImageFormat('jpg');
            $imagick->writeImage($thumbUrl);
            $url = $thumbUrl;
        }

        return new BinaryFileResponse(new File($url));
    }

    public function downloadAction($id){

        //Get required file
        $document   = $this->getDoctrine()->getRepository(Document::class)->find($id);

        //Renommage du nom pour éviter les problèmes au téléchargement d'un fichier
        $name       = str_replace(DocumentController::$filenameReplaceArray, '_', $document->getName() );

        //Create response with requested file
        $file       = new File($document->getWebPath());
        $response   = new BinaryFileResponse(new File($document->getWebPath()));

        //Configure response
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $name.".".$file->guessExtension());
        return $response;
    }

    public function removeAction($id){

        $em 	 	= $this->getDoctrine()->getManager();
        $document	= $em->getRepository(Document::class)->find($id);

        $thumbUrl = 'bundles/kaliticsged/images/thumbnail/thumb-'.$document->getId().'.jpg';

        if (file_exists($thumbUrl)){
           unlink($thumbUrl);
        }

        //Delete document
        $em	->remove($document);
        $em	->flush();

        return new JsonResponse(array(), Response::HTTP_OK);
    }

    public function updateAction(Request $request, $id){

        $em 	 	= $this->getDoctrine()->getManager();
        $document	= $em->getRepository(Document::class)->find($id);
        $types      = array();

        //Récupération des types de fichiers
        $category   = $document->getGed()->getCategory();
        if($category != null){
            $types = $em->getRepository(DocumentType::class)->findBy(array(
                "category" => $category
            ), ['name' => 'ASC']);
        }else{
            $types = $em->getRepository(DocumentType::class)->findBy([], ['name' => 'ASC']);
        }

        $form       = $this->createForm(
            FormDocument::class,
            $document,
            [
                "action" => $this->generateUrl(
                    'kalitics_ged_document_update',
                    array('id' => $id)
                ),
                "documentsTypes" => $types
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($document);
            $entityManager->flush();
            return new JsonResponse(array(), Response::HTTP_OK);
        }

        return $this->render('@KaliticsGed/_includes/_form_update.html.twig', array(
            'form'  => $form->createView(),
            'ged'   => $document->getGed()
        ));
    }

    private function saveFile(UploadedFile $file){
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move('uploads/documents/', $fileName);
        return 'uploads/documents/'.$fileName;
    }

    private function getRessourceUrl(Document  $document) : string
    {
        switch ($document->getExtension()) {
            case 'png':
            case 'jpg':
            case 'jpeg':
                if (file_exists($document->getPath())){
                    $url = $document->getWebPath();
                }else{
                    $url = 'bundles/kaliticsged/images/jpg.png';
                }
                break;
            case 'pdf':
                if (file_exists($document->getPath())){
                    $url = $document->getWebPath();
                }else{
                    $url = 'bundles/kaliticsged/images/pdf.png';
                }
                break;
            default:
                $url = 'bundles/kaliticsged/images/doc.png';
        }

        return $url;
    }

    private function getDirectoryPath(Filesystem $filesystem) : string {
        $adapter = $filesystem->getAdapter();
        $reflection = new \ReflectionClass($adapter);
        $directory = $reflection->getProperty('directory');
        $directory->setAccessible(true);
        return $directory->getValue($adapter);
    }
}
