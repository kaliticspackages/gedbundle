function initFancyBox()
{
    console.log("init fancy box")
    $("a.pdf-fancy").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'overlayShow'	:	true,
        'type'          :   'iframe'
    })
}


function changeNbExportFile() {
    let nbExportFile = 0;

    $(".exportFile").each(function (){
        if ($(this).is(':checked')){
            nbExportFile++;
        }
    });

    if (nbExportFile === 0){
        $("#nbFileExport").html("");
    }else{
        $("#nbFileExport").html(`(${nbExportFile})`);
    }
}

function exportFiles(){
    let files = [];
    $(".exportFile").filter(':checked').each(function (){ files.push($(this).data('document')) });

    for (var i = files.length - 1; i >= 0; i--) {
        var a = document.createElement("a");
        a.target = "_blank";
        a.download = "download";
        a.href = files[i];
        a.click();
    }
}

function resizeTile(e){
    let $isPdf = $(e).parent().parent().data('ispdf') == true
    let docId = $(e).data('doc-id');
    let size  = $(e).data('size');
    let $gridItem = $(`.grid-item-${docId}`);
    let col = 0;
    let row = 0;

    switch (size) {
        case 'small':
            col = 3;
            row = 3
            break;
        case 'medium':
            col = $isPdf ? 4 : 6;
            row = 6; //6
            break;
        case 'big':
            col = $isPdf ? 6 : 9;
            row = $isPdf ? 9 : 6;
            break;
    }

    $gridItem.removeAttr('style');
    $gridItem.css('grid-column',`span ${col}`)
    $gridItem.css('grid-row',`span ${row}`)
}

function printDocument(element){
    let $element = $(element);
    let path = $element.data('path')
    let docType = $element.data('doctype')

    printJS({
        printable: path,
        type: docType,
    })
}

$(function (){
    initFancyBox();
});

