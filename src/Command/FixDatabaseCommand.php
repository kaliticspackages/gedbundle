<?php

namespace Kalitics\GedBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Kalitics\GedBundle\Entity\ConfigGedCategory;
use Kalitics\GedBundle\Entity\HasGedInterface;
use Kalitics\GedBundle\Repository\ConfigGedCategoryRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use ReflectionClass;
use ReflectionException;

class FixDatabaseCommand
    extends Command
{
    protected static $defaultName = 'kalitics:ged:fix-database';

    private ConfigGedCategoryRepository $configGedCategoryRepository;
    private EntityManagerInterface      $entityManager;
    private SymfonyStyle                $symfonyStyle;

    /**
     * FixDatabaseCommand constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface                       $entityManager
     * @param string|null                                                $name
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        string $name = null
    ) {
        $this->configGedCategoryRepository = $entityManager->getRepository(ConfigGedCategory::class);
        $this->entityManager               = $entityManager;

        parent::__construct(
            $name
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function initialize(
        InputInterface $input,
        OutputInterface $output
    ) {
        parent::initialize(
            $input,
            $output
        );

        $this->symfonyStyle = new SymfonyStyle(
            $input,
            $output,
        );

        $this->symfonyStyle->title(self::$defaultName);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) : int {
        $interface = HasGedInterface::class;

        $configs = $this->configGedCategoryRepository->findAll();

        /** @var \Kalitics\GedBundle\Entity\ConfigGedCategory $config */
        foreach ($configs as $config) {
            $id = $config->getId();
            $configurationGedCategory = $this->configGedCategoryRepository->find($id);

            $category    = $configurationGedCategory->getCategory();
            $entityField = $configurationGedCategory->getEntityField();

            $this->symfonyStyle->section("Entity '{$entityField}'");

            try {
                $reflectionClass = new ReflectionClass($entityField);
            } catch (ReflectionException $reflectionException) {
                $this->symfonyStyle->error("Class '{$entityField}' MUST exist");

                continue;
            }

            if (!$reflectionClass->implementsInterface($interface)) {
                $this->symfonyStyle->error("Entity '{$entityField}' MUST implement interface '{$interface}'");

                continue;
            }

            $queryBuilder = $this->entityManager
                ->createQueryBuilder()
                ->select('Entity')
                ->from(
                    $entityField,
                    'Entity'
                )
                ->innerJoin(
                    'Entity.ged',
                    'GED'
                )
                ->andWhere('Entity.ged IS NOT NULL')
                ->andWhere(
                    "
                GED.category != :category
                OR
                GED.config != :config
                OR
                GED.category IS NULL
                OR
                GED.config IS NULL
                "
                )
                ->setParameter(
                    'category',
                    $category
                )
                ->setParameter(
                    'config',
                    $configurationGedCategory
                );

            $count = self::countQueryBuilder($queryBuilder);
            if ($count > 0) {
                $this->symfonyStyle->progressStart($count);

                $iterator = $queryBuilder
                    ->getQuery()
                    ->iterate();
                foreach ($iterator as $row) {
                    /** @var HasGedInterface $entity */
                    $entity = $row[0];
                    $ged    = $entity->getGed();

                    $ged->setCategory($category);
                    $ged->setConfig($configurationGedCategory);

                    $this->entityManager->persist($ged);
                }

                $this->entityManager->flush();
                $this->entityManager->clear();

                $this->symfonyStyle->progressFinish();
            } else {
                $this->symfonyStyle->note("No entity to fix");
            }
        }

        return 0;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     *
     * @return int
     */
    private static function countQueryBuilder(
        QueryBuilder $queryBuilder
    ): int {
        $paginator = new Paginator(
            $queryBuilder,
        );

        return $paginator
            ->setUseOutputWalkers(false)
            ->count();
    }
}
