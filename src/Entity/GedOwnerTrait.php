<?php

namespace Kalitics\GedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait inherited by all owners of a GED (Gestion electronique de document)
 */
trait GedOwnerTrait
{
    /**
     * @ORM\OneToOne(targetEntity="Kalitics\GedBundle\Entity\Ged", cascade={"persist"})
     * @ORM\JoinColumn(name="ged_id", referencedColumnName="id")
     */
    private $ged;

    /**
     * @return mixed
     */
    public function getGed()
    {
        return $this->ged;
    }

    /**
     * @param mixed $ged
     */
    public function setGed($ged)
    {
        $this->ged = $ged;
    }
}
