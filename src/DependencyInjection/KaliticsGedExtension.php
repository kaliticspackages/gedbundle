<?php

declare(strict_types = 1);

namespace Kalitics\GedBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class KaliticsGedExtension extends Extension{

    /**
     * @param array                                                   $configs
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     *
     * @return void
     * @throws \Exception
     */
    public function load(
        array $configs,
        ContainerBuilder $container
    ) : void {
        $fileLocator = new FileLocator(
            __DIR__ . '/../Resources/config',
        );
        $loader      = new YamlFileLoader(
            $container,
            $fileLocator,
        );
        $loader->load('services.yaml');
    }
}
